package mymath

import (
	"testing"
)

func TestCeil(t *testing.T) {
	type testData struct {
		a        float64
		expected float64
	}
	testCases := []testData{
		{a: 5, expected: 5},
		{a: 6, expected: 6},
	}
	for _, tc := range testCases {
		result := Ceil(tc.a)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}

func TestYn(t *testing.T) {
	type testData struct {
		a        int
		d        float64
		expected float64
	}
	testCases := []testData{
		{a: 1, d: 2, expected: -0.10703243154093756},
		{a: 6, d: 3, expected: -5.436470340703773},
	}
	for _, tc := range testCases {
		result := Yn(tc.a, tc.d)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
func TestSqrt(t *testing.T) {
	type testData struct {
		a        float64
		expected float64
	}
	testCases := []testData{
		{a: 4, expected: 2},
		{a: 9, expected: 3},
	}
	for _, tc := range testCases {
		result := Sqrt(tc.a)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
func TestSFloor(t *testing.T) {
	type testData struct {
		a        float64
		expected float64
	}
	testCases := []testData{
		{a: 5, expected: 5},
		{a: 9, expected: 9},
	}
	for _, tc := range testCases {
		result := Floor(tc.a)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
func TestSAbs(t *testing.T) {
	type testData struct {
		a        float64
		expected float64
	}
	testCases := []testData{
		{a: 5, expected: 5},
		{a: 9, expected: 9},
	}
	for _, tc := range testCases {
		result := Abs(tc.a)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
func TestPow(t *testing.T) {
	type testData struct {
		a        float64
		d        float64
		expected float64
	}
	testCases := []testData{
		{a: 5, d: 2, expected: 25},
		{a: 3, d: 3, expected: 27},
	}
	for _, tc := range testCases {
		result := Pow(tc.a, tc.d)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
func TestMax(t *testing.T) {
	type testData struct {
		a        float64
		d        float64
		expected float64
	}
	testCases := []testData{
		{a: 5, d: 2, expected: 5},
		{a: 3, d: 8, expected: 8},
	}
	for _, tc := range testCases {
		result := Max(tc.a, tc.d)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
func TestMin(t *testing.T) {
	type testData struct {
		a        float64
		d        float64
		expected float64
	}
	testCases := []testData{
		{a: 5, d: 2, expected: 2},
		{a: 3, d: 8, expected: 3},
	}
	for _, tc := range testCases {
		result := Min(tc.a, tc.d)
		if result != tc.expected {
			t.Errorf("Expected: %v, Got: %v", tc.expected, result)
		}
	}
}
